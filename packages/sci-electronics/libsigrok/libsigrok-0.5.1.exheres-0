# Copyright 2013-2018 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# python interpreter is needed to build python and C++ bindings,
# but requested by the build system even when bindings are not built
require python [ blacklist=none multibuild=false ]
require ruby [ blacklist=none multibuild=false with_opt=true ]
require freedesktop-mime
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Sigrok hardware driver library"
HOMEPAGE="https://sigrok.org"
DOWNLOADS="${HOMEPAGE}/download/source/${PN}/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    cxx [[ description = [ Build C++ bindings ] ]]
    java [[
        description = [ Build Java bindings ]
        requires = cxx
    ]]
    python [[
        description = [ Build Python bindings ]
        requires = cxx
    ]]
    ruby [[
        description = [ Build Ruby bindings ]
        requires = cxx
    ]]

    asix-sigma [[ description = [ Add support for the ASIX Sigma logic analyzer ] ]]
    chronovu-la [[ description = [ Add support for the ChronoVu LA ] ]]
    ftdi-la [[ description = [ Add support for FTDI chip based logic analyzers ] ]]
    hung-chang-dso-2100 [[ description = [ Add support for the Hung-Chang DSO-2100 ] ]]
    ikalogic-scanaplus [[ description = [ Add support for the Ikalogic ScanaPlus ] ]]
    pipistrello-ols [[ description = [ Add support for the Pipistrello-OLS ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.22]
        cxx? ( app-doc/doxygen )
        java? ( dev-lang/swig )
        python? (
            dev-lang/swig[python]
            dev-python/setuptools[python_abis:*(-)?]
        )
        ruby? ( dev-lang/swig[>=3.0.8][ruby][ruby_abis:*(-)?] )
    build+run:
        app-arch/libzip[>=0.10]
        dev-libs/glib:2[>=2.32.0]
        dev-libs/libserialport[>=0.1.1]
        dev-libs/libusb:1[>=1.0.16]
        cxx? ( gnome-bindings/glibmm:2.4[>=2.32.0][-disable-deprecated] )
        java? (
            dev-lang/swig
            virtual/jdk:=
        )
        python? (
            dev-python/numpy[python_abis:*(-)?]
            gnome-bindings/pygobject:3[>=3.0.0][python_abis:*(-)?]
        )
        (
            asix-sigma? (
                dev-libs/libftdi:1[>=1.0]
                sys-libs/zlib[>=1.2.3.1]
            )
            chronovu-la? ( dev-libs/libftdi:1[>=1.0] )
            ftdi-la? ( dev-libs/libftdi:1[>=1.0] )
            hung-chang-dso-2100? ( sys-libs/libieee1284 )
            ikalogic-scanaplus? ( dev-libs/libftdi:1[>=1.0] )
            pipistrello-ols? ( dev-libs/libftdi:1[>=1.0] )
        ) [[ note = [ Device specific dependencies ] ]]
    test:
        dev-libs/check[>=0.9.4]
    suggestion:
        sci-electronics/sigrok-firmware-fx2lafw-bin [[
            description = [ Required firmware files to use Cypress FX2 based devices ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-build-Use-AX_RUBY_EXT-macro.patch
)
DEFAULT_SRC_CONFIGURE_PARAMS=(
    # drivers with no external dependencies
    '--enable-demo'

    '--enable-baylibre-acme'
    '--enable-beaglelogic'
    '--enable-hp-3457a'
    '--enable-lecroy-xstream'
    '--enable-maynuo-m97'
    '--enable-rigol-ds'
    '--enable-scpi-pps'
    '--enable-yokogawa-dlm'
    '--enable-fluke-45'
    '--enable-ipdbg-la'
    '--enable-siglent-sds'

    # drivers dependent on libserialport
    '--enable-agilent-dmm'
    '--enable-appa-55ii'
    '--enable-arachnid-labs-re-load-pro'
    '--enable-atten-pps3xxx'
    '--enable-brymen-dmm'
    '--enable-cem-dt-885x'
    '--enable-center-3xx'
    '--enable-colead-slm'
    '--enable-conrad-digi-35-cpu'
    '--enable-fluke-dmm'
    '--enable-gmc-mh-1x-2x'
    '--enable-gwinstek-gds-800'
    '--enable-hameg-hmo'
    '--enable-kern-scale'
    '--enable-korad-kaxxxxp'
    '--enable-manson-hcs-3xxx'
    '--enable-mic-985xx'
    '--enable-motech-lps-30x'
    '--enable-norma-dmm'
    '--enable-openbench-logic-sniffer'
    '--enable-pce-322a'
    '--enable-rohde-schwarz-sme-0x'
    '--enable-serial-dmm'
    '--enable-serial-lcr'
    '--enable-teleinfo'
    '--enable-tondaj-sl-814'
    '--enable-gwinstek-gpd'
    '--enable-rdtech-dps'
    '--enable-zketech-ebd-usb'

    # drivers dependent on libusb
    '--enable-brymen-bm86x'
    '--enable-fx2lafw'
    '--enable-hantek-6xxx'
    '--enable-hantek-dso'
    '--enable-ikalogic-scanalogic2'
    '--enable-kecheng-kc-330b'
    '--enable-lascar-el-usb'
    '--enable-lecroy-logicstudio'
    '--enable-saleae-logic16'
    '--enable-sysclk-lwla'
    '--enable-uni-t-dmm'
    '--enable-uni-t-ut32x'
    '--enable-victor-dmm'
    '--enable-saleae-logic16'
    '--enable-zeroplus-logic-cube'
    '--enable-dreamsourcelab-dslogic'
    '--enable-hantek-4032l'
    '--enable-saleae-logic-pro'

    # libgpib
    '--disable-hp-3478a'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'cxx'
    'java'
    'python'
    'ruby'

    'asix-sigma'
    'chronovu-la'
    'ftdi-la'
    'hung-chang-dso-2100'
    'ikalogic-scanaplus'
    'pipistrello-ols'
)

AT_M4DIR=( 'm4' )

src_prepare() {
    default

    # We need version 4 of this macro which will be shipped
    # with the next autoconf-archive release after v2017.03.21
    edo cp "${FILES}"/ax_ruby_ext.m4 "${WORK}"/m4

    eautoreconf

    option ruby && export RUBY=ruby$(ruby_get_abi)
}

