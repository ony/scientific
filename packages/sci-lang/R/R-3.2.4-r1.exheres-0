# Copyright (c) 2008, 2010 Stephen P. Becker <spbecker@exherbo.org>
# Copyright 2014 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2015 Stephen P. Becker <spbecker@gmail.com>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="The R project for statistical computing."
HOMEPAGE="http://www.r-project.org"
DOWNLOADS="http://cran.r-project.org/src/base/${PN}-$(ever major)/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cairo
    jpeg
    png
    readline
    shared [[ description = [ Build R as a shared library (results in a 10% performance penalty). ] ]]
    tcltk
    tiff
    X

    jpeg? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.16.1]
        cairo? ( virtual/pkg-config )
    build+run:
        app-arch/xz
        dev-libs/pcre
        dev-libs/tre
        sys-libs/libgfortran:=
        sys-libs/zlib
        virtual/blas
        virtual/lapack
        cairo? (
            dev-libs/glib
            x11-libs/cairo[>=1.2]
            x11-libs/pango
        )
        jpeg? (
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        )
        png? ( media-libs/libpng:= )
        readline? ( sys-libs/readline:= )
        tcltk? ( dev-lang/tk )
        tiff? ( media-libs/tiff )
        X? (
            x11-libs/libX11
            x11-libs/libXmu
            x11-libs/libXt
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    rdocdir=/usr/share/${PN}/share/doc/
    rincludedir=/usr/$(exhost --target)/${PN}/include/
    rsharedir=/usr/share/${PN}/share/
    --with-recommended-packages
    --enable-threads=posix
    --enable-nls
    --exec-prefix=/usr/$(exhost --target)
    --prefix=/usr
    --with-blas
    --with-lapack
    --with-system-bzlib
    --with-system-pcre
    --with-system-tre
    --with-system-xz
    --with-system-zlib
    --without-ICU
    --without-included-gettext
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "shared R-shlib" )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    cairo
    "jpeg jpeglib"
    "png libpng"
    readline
    tcltk
    "tiff libtiff"
    "X x"
)

src_configure() {
    export F77=$(exhost --tool-prefix)gfortran

    default
}

src_test() {
    unset R_HOME

    default
}

src_install() {
    default

    option shared && hereenvd 60R <<EOF
LDPATH="/usr/$(exhost --target)/lib/R/lib"
R_HOME="/usr/$(exhost --target)/lib/R"
EOF
}

